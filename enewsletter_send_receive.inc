<?php

/**
 * @file
 * Send emails.
 */

function enewsletter_process_publications($type) {
  global $base_url;
  $publications = publication_select_publications($type);

  if (empty($publications)) {
    return;
  }

  // Configuration settings.
  $site_name = check_plain(variable_get('site_name', 'drupal'));
  $process_count = variable_get('deliver_send_maximum', 250);
 
  // Get profile field names.
  $profile_fields = enewsletter_profile_fields('fid', 'name');
 
  foreach ($publications as $publication) {
	
    $schedules = schedule_select_schedules($type, $publication->publication_id);
    $publication_terms = publication_select_allowed_terms($publication->publication_id);   

    foreach ($schedules as $schedule) {
      $action_id = schedule_action_select_db($type, $publication->publication_id, $schedule['schedule_id']);

      // Get nodes.
      if (empty($publication_terms)) {
        $nodes_by_term = publication_get_nodes($type, $schedule['previous'], $schedule['last']);
      }
      else {
        $nodes_by_term = publication_get_nodes_and_terms($type, $publication_terms, $schedule['previous'], $schedule['last']);
      }

      $terms_allowed = publication_terms_allowed_list($nodes_by_term);
      $nodes_allowed = publication_nodes_allowed_list($nodes_by_term);
      $nodes_allowed = publication_format_nodes($nodes_allowed);
      $send_ready = subscribed_send_select_db($type, $process_count, $schedule['schedule_id']);
      $template_revision_ids = array();

      while ($send = db_fetch_array($send_ready)) {
 
        if (empty($nodes_by_term) && empty($publication->send_empty)) {
          subscribed_send_update_db($send['pub_time'], $schedule['next'], $send['type'], $send['uid'], $publication->publication_id, $send['schedule_id'], $schedule['frequency']);
          break;
        }

        if (empty($template_revision_ids[$send['pub_time']])) {
          $template_revision_ids[$send['pub_time']] = schedule_revision_select_db($type, $publication->publication_id, $send['schedule_id'], $send['pub_time']);
        }

        // Get templates.
        $templates = templates_load_templates('schedule', $schedule['schedule_id'], $template_revision_ids[$send['pub_time']]);

        $template = ($send['markup'] == 'html') ? $templates['html'] : $templates['text'];

        // Get terms and nodes.
        $terms_selected = subscribed_terms_selected_select_db($type, $publication->publication_id, $send['uid']);

        $nodes = publication_get_selected_nodes($terms_selected, $terms_allowed, $nodes_allowed);
        $nodes = publication_process_nodes($nodes, $send['hash'], $send['markup'], $publication->publication_id, $schedule['schedule_id'], $action_id);

        // Get profile.
        $profile = enewsletter_load_profile($send['uid']);
        $user_details = subscribed_subscription_user_details_select($send['uid']);
        // url() is not used here because it will prefix the path with '/'
        // and TinyMCE does that automatically as well, resulting in '//'
        $forward = 'forward_email/' .  $user_details['hash'] . '/' . $publication->publication_id . '/' . $send['schedule_id'] . '/0';

        // Process.
        $subject = publication_title_process($publication->subject);     
        $content = enewsletter_load_content($site_name, $subject, $publication, $template, $user_details, $forward, $profile, $nodes);
        $headers = array(
          'X-Newsletter' => $send['publication_id'],
        );

        // Convert if subscription markup is plain text.
        if ($send['markup'] == 'html') {
          $content = preg_replace('/<a href="([^"]+)"[^>]*>(.+?)<\/a>/ie', 'html2txt_links_make_absolute("\\1", "\\2")', $content);
          $content = preg_replace('/src="([^"]+)"/ie', 'html2txt_image_absolute("\\1")', $content);
        }
        else {
          $converted = html2txt_convert($content, 70, FALSE);
          $content = $converted->text . $converted->links;
        }

        // Send.
        $email_sent = deliver_send_email('enewletter-publication', $send['mail'], $subject, $content, $send['markup'], $headers);
        
        // Make record of send.
        if ($email_sent) {
          subscribed_send_update_db($send['pub_time'], $schedule['next'], $send['type'], $send['uid'], $publication->publication_id, $send['schedule_id'], $schedule['frequency']);
          subscribed_sent_insert_db($send['uid'], $publication->publication_id, $schedule['schedule_id'], $send['pub_time']);
          $process_count = $process_count - 1;
					if ($process_count == 0) return;   
          usleep(variable_get('subscribed_send_delay', 2));
        }
      }
    }
  }
}

/**
 * 
 */
function enewsletter_load_profile($uid) {
  if (!module_exists('profile')) return;
	$result = db_query('SELECT f.fid, f.name, f.type, v.value FROM {profile_fields} f INNER JOIN {profile_values} v ON f.fid = v.fid WHERE uid = %d', $uid);

  while ($field = db_fetch_object($result)) {
    $user[$field->fid] = _profile_field_serialize($field->type) ? unserialize($field->value) : $field->value;
  }

  return $user;
}

/**
 * 
 */
function enewsletter_load_newsletter_templates($type, $templates) {
  foreach ($templates as $markup => $nid) {
    $template = templates_get_templates($type, $nid);
    $template_used[$template['markup']] = $template['template'];
    $template_used[$template['markup'] .'_markup'] = $template['markup'];
  }

  return $template_used;
}


/**
 * Fill template with newsletter/user data.
 */
function enewsletter_load_content($site_name, $subject, $publication, $template, $subscription, $forward, $profile = NULL, $nodes = NULL) {
  $publication->nodes = $nodes;

  $subscription = (object) $subscription;
  $subscription->publication = $publication;
  $subscription->forward = $forward;
  
  $objects = array(
    'user' => user_load(array('uid' => $subscription->uid)),
    'publication' => $publication,
    'subscribed' => $subscription,
  );

  $content = check_markup($template['body'], $template['markup'], FALSE);
  $content = templates_process_template($template['body'], $objects);

  if ($template['markup'] == 'html') {
    $html_header = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
                    <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
                    <meta name="robots" content="none" />
                    <title></title>';
    if (!empty($template['css'])) {
      $html_header .= '<style type="text/css">'. check_plain($template['css']) .'</style>';
    }

    $html_header .= '</head>
                     <body>';

    $content = $html_header . $content .'<body></html>';
  }

  return $content;
}

/**
 * Used to encodes mail headers that contain non US- ASCII
 * characters.
 * http://www.rfc-editor.org/rfc/rfc2047.txt
 *
 * Notes:
 *   - The chunks come in groupings of 4 bytes when using base64
 *     encoded.
 *   - trim() is used to ensure that no extra spacing is added by
 *     chunk_split() or preg_replace().
 *   - Using \n as the chunk separator may cause problems on some
 *     systems and may have to be changed to \r\n or \r.
 */
function enewsletter_email_encode($string, $charset = "UTF-8") {
  $chunk_size = 75 - 7 - strlen($charset);
  $chunk_size -= $chunk_size % 4;
  $string = trim(chunk_split(base64_encode($string), $chunk_size, "\n"));
  $string = trim(preg_replace('/^(.*)$/m', " =?$charset?B?\\1?=", $string));
  return $string;
}

/**
 * 
 */
function enewsletter_data_cleanup($field, $value) {
  switch ($field->type) {
    case 'textfield':
    case 'selection':
    case 'list':
    case 'url':
      return check_plain($value);       
    case 'textarea':
      return check_markup($value);
    case 'checkbox':
      $bool = (empty($value)) ? t('False') : t('True');
      return $bool; 
    case 'date':
      list($format) = explode(' - ', 'd/m/Y', 2);
      // Note: we avoid PHP's date() because it does not handle dates before
      // 1970 on Windows. This would make the date field useless for e.g.
      // birthdays.
      $replace = array(
        'd' => sprintf('%02d', $value['day']),
        'j' => $value['day'],
        'm' => sprintf('%02d', $value['month']),
        'M' => _profile_map_month($value['month']),
        'Y' => $value['year']
      );
      $date = strtr($format, $replace);
      return $date;
  }
}

/**
 * Diagnostics.
 */
// function enewsletter_watchdog($watchdog) {
//   $log = "Processing: '". $watchdog['subject'] ."' Subscribed: ". $watchdog['subscribers']; 
//   watchdog('enewsletter', $log);
// 
//   if (!is_array($watchdog['schedules'])) {
//     return;
//   }
// 
//   foreach ($watchdog['schedules'] as $schedule) {
//     $log = $watchdog['subject'] .' ('. $schedule['subject'] .'): '. $schedule['sent'] .' sent'; 
//     watchdog('enewsletter', $log);
//   }
// } 

/**
 * 
 */
function enewsletter_content_load_address_variables($values, $variables_string = NULL) {
  if (!module_exists('address') || empty($values)) {
    return $variables_string;
  }

  while (list($field, $data) = each($values)) {
    $prefix = substr($field, 0, 8);
 
    if ($prefix == 'address_') {
      $processed_data = check_plain($data);   
    }
    
    if (!empty($processed_data)) {
      $variables_string .= '$'. $field ." = '". addslashes($processed_data) ."';";
    }
    unset($processed_data);
  }

  return $variables_string;
}

/**
 * 
 */
function enewsletter_content_load_profile_variables($values, $variables_string = NULL) {
  if (empty($values) || !module_exists('profile')) {
    return $variables_string;
  }

  $profile_fields = enewsletter_profile_fields();

  // Load user's profile data.
  while (list($fid, $field) = each($profile_fields)) {
    if (!empty($values[$fid])) {
      $processed_data = enewsletter_data_cleanup($field, $values[$fid]);
    }

    $variables_string .= (empty($processed_data)) ? '$'. $field->name ." = '';" : '$'. $field->name ." = '". addslashes($processed_data) ."';";
    unset($processed_data);
  }

  return $variables_string;
}

/**
 * Replace any variables that are still in the content.
 */
function enewsletter_content_profile_variables_replace($content, $profile) {
  if (!module_exists('profile')) {
    return $content;
  }

  $profile_fields = enewsletter_profile_fields();

  while (list($fid, $field) = each($profile_fields)) {
    $clean_data = enewsletter_data_cleanup($field, $profile[$fid]);
    $content = str_replace('$'. $field->name, $clean_data, $content);
    unset($clean_data);
  }
  
  return $content;
}

/**
 * Replace  any variables that are still in the content.
 */
function enewsletter_content_address_variables_replace($content, $address) {
  if (!module_exists('address')) {
    return $content;
  }

  while (list($field, $data) = each($address)) {
    $prefix = substr($field, 0, 8);

    if ($prefix == 'address_') {
      $clean_data = check_plain($data);
      $content = str_replace('$'. $field, addslashes($clean_data), $content);
      unset($clean_data);
    }
  }
  
  return $content;
}
