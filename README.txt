********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: Enewsletter Module
Author: Robert Castelo
Drupal: 6.x
********************************************************************
DESCRIPTION:

Publish the content of a website as an email newsletter. 

Features include:

 - Multiple email newsletters, set up as many as you need.
 - Plain text or HTML  email (subscriber chooses which to receive).
 - Include nodes/teasers in newsletter based on vocabulary terms.
 - Subscriber can select which of the allowed terms they receive nodes from.
 - Manual or scheduled send out - every (x) hours, (x) days, or (x) months.
 - Personalisation, profile info is added into email.
 - Templates can use Drupal input formats, e.g. use PHP to create a conditional plain text email
 - Bounced email handling.
 - Email link authentication. 'Read more' links, and account control links, allow users to access the 
   site (based on their role) and their email subscription settings, but without accessing their full site account.
 - 100% built for Drupal.


********************************************************************
PREREQUISITES:

Access Perm Grouping Module
http://drupal.org/project/access_perm_group

Bounced Email  Module
http://drupal.org/project/bounced_email

Delivery  Module
http://drupal.org/project/delivery

html2txt  Module
http://drupal.org/project/html2txt

Identity Hash  Module
http://drupal.org/project/identity_hash

Publication  Module
http://drupal.org/project/publication

Schedule  Module
http://drupal.org/project/schedule

Subscribed  Module
http://drupal.org/project/subscribed

Templates  Module
http://drupal.org/project/templates



********************************************************************
INSTALLATION:

Note: It is assumed that you have Drupal up and running.  Be sure to
check the Drupal web site if you need assistance.  If you run into
problems, you should always read the INSTALL.txt that comes with the
Drupal package and read the online documentation.

1. Place the entire directory of this module into your Drupal directory:
    sites/all/modules/

2. Enable the module by navigating to:
    administer > build > modules

Click the 'Save configuration' button at the bottom to commit your
changes. 



********************************************************************
AUTHOR CONTACT

- Report Bugs/Request Features:
   http://drupal.org/project/enewsletter
   
- Comission New Features:
   http://www.codepositive.com/contact
   
- Want To Say Thank You:
   http://www.amazon.com/gp/registry/O6JKRQEQ774F

        
********************************************************************
ACKNOWLEDGEMENT

Developed by Robert Castelo for Code Positive <http://www.codepositive.com>

